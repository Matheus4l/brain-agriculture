import { DateTime } from 'luxon'
import { BaseModel, belongsTo, column, manyToMany } from '@adonisjs/lucid/orm'
import type { BelongsTo, ManyToMany } from '@adonisjs/lucid/types/relations'
import RuralProducer from './rural_producer.js'
import CultivatedCrop from './cultivated_crop.js'

export default class Farm extends BaseModel {
  @column({ isPrimary: true })
  declare id: number

  @column()
  declare name: string

  @column()
  declare ruralProducerId: number
  @belongsTo(() => RuralProducer)
  declare ruralProducer: BelongsTo<typeof RuralProducer>

  @manyToMany(() => CultivatedCrop, {
    pivotTable: 'farm_cultivated_crop_associations',
  })
  declare cultivatedCrops: ManyToMany<typeof CultivatedCrop>

  @column()
  declare total_area_hectares: number

  @column()
  declare agricultural_area_hectares: number

  @column()
  declare vegetation_area_hectares: number

  @column()
  declare cityName: string

  @column()
  declare stateName: string

  @column.dateTime({ autoCreate: true })
  declare createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  declare updatedAt: DateTime
}
