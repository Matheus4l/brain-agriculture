import { DateTime } from 'luxon'
import { BaseModel, column, manyToMany } from '@adonisjs/lucid/orm'
import Farm from './farm.js'
import type { ManyToMany } from '@adonisjs/lucid/types/relations'

export default class CultivatedCrop extends BaseModel {
  @column({ isPrimary: true })
  declare id: number

  @column()
  declare name: string

  @manyToMany(() => Farm, {
    pivotTable: 'farm_cultivated_crop_associations',
  })
  declare farms: ManyToMany<typeof Farm>

  @column.dateTime({ autoCreate: true })
  declare createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  declare updatedAt: DateTime
}
