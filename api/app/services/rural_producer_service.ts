// app/Services/RuralProducerService.ts

import RuralProducer from '../models/rural_producer.js'

export default class RuralProducerService {
  /**
   * Método para criar um novo produtor rural.
   * @param data - Os dados do novo produtor rural a ser criado.
   * @returns O novo produtor rural criado.
   */
  async create(data: Partial<RuralProducer>): Promise<RuralProducer | string> {
    // Verifica se o CPF ou CNPJ está definido
    const cpfOrCnpj = data.cpf_or_cnpj || ''

    // Verifica se o produtor rural já existe na base de dados
    const existingRuralProducer = await this.findByCpfOrCnpj(cpfOrCnpj)
    if (existingRuralProducer) {
      return 'CPF or CNPJ existing'
    }

    // Se o CPF ou CNPJ não existir na base de dados, cria um novo produtor rural
    return await RuralProducer.create(data)
  }

  /**
   * Método para atualizar um produtor rural existente.
   * @param id - O ID do produtor rural a ser atualizado.
   * @param data - Os dados atualizados do produtor rural.
   * @returns O produtor rural atualizado.
   */
  async update(id: number, data: Partial<RuralProducer>): Promise<RuralProducer> {
    const ruralProducer = await RuralProducer.findOrFail(id)
    ruralProducer.merge(data)
    await ruralProducer.save()
    return ruralProducer
  }

  /**
   * Método para excluir um produtor rural existente.
   * @param id - O ID do produtor rural a ser excluído.
   */
  async delete(id: number): Promise<void | string> {
    if (!id) {
      return 'Rural producer ID is required'
    }
    console.log(id)

    // Busca o produtor rural pelo ID
    const ruralProducer = await this.getById(id)
    // Verifica se o produtor rural existe
    if (!ruralProducer) {
      return 'Rural producer not found'
    }
    await ruralProducer.delete()
  }

  /**
   * Método para buscar todos os produtores rurais.
   * @returns Uma lista de todos os produtores rurais.
   */
  async getAllPaginated(page: number, perPage: number) {
    // Obtém a lista de produtores rurais paginados do banco de dados
    const ruralProducers = await RuralProducer.query().paginate(page, perPage)

    return ruralProducers
  }

  /**
   * Método para buscar um produtor rural pelo ID.
   * @param id - O ID do produtor rural a ser buscado.
   * @returns O produtor rural encontrado.
   */
  async getById(id: number): Promise<RuralProducer | null> {
    return await RuralProducer.findBy('id', id)
  }

  /**
   * Verifica se um produtor rural com o CPF ou CNPJ especificado já existe na base de dados.
   * @param cpfOrCnpj - O CPF ou CNPJ a ser verificado.
   * @returns O produtor rural existente, se encontrado. Caso contrário, null.
   */
  private async findByCpfOrCnpj(cpfOrCnpj: string): Promise<RuralProducer | null> {
    return await RuralProducer.findBy('cpf_or_cnpj', cpfOrCnpj)
  }
}
