// app/Services/CultivatedCropService.ts

import CultivatedCrop from '../models/cultivated_crop.js'

export default class CultivatedCropService {
  /**
   * Método para criar uma nova cultura cultivada.
   * @param data - Os dados da nova cultura cultivada a ser criada.
   * @returns A nova cultura cultivada criada.
   */
  async create(data: Partial<CultivatedCrop>): Promise<CultivatedCrop> {
    return await CultivatedCrop.create(data)
  }

  /**
   * Método para atualizar uma cultura cultivada existente.
   * @param id - O ID da cultura cultivada a ser atualizada.
   * @param data - Os dados atualizados da cultura cultivada.
   * @returns A cultura cultivada atualizada.
   */
  async update(id: number, data: Partial<CultivatedCrop>): Promise<CultivatedCrop> {
    const cultivatedCrop = await CultivatedCrop.findOrFail(id)
    cultivatedCrop.merge(data)
    await cultivatedCrop.save()
    return cultivatedCrop
  }

  /**
   * Método para excluir uma cultura cultivada existente.
   * @param id - O ID da cultura cultivada a ser excluída.
   */
  async delete(id: number): Promise<void> {
    const cultivatedCrop = await CultivatedCrop.findOrFail(id)
    await cultivatedCrop.delete()
  }

  /**
   * Método para buscar todas as culturas cultivadas.
   * @returns Uma lista de todas as culturas cultivadas.
   */
  async getAll(): Promise<CultivatedCrop[]> {
    return await CultivatedCrop.all()
  }

  /**
   * Método para buscar uma cultura cultivada pelo ID.
   * @param id - O ID da cultura cultivada a ser buscada.
   * @returns A cultura cultivada encontrada.
   */
  async getById(id: number): Promise<CultivatedCrop> {
    return await CultivatedCrop.findOrFail(id)
  }
}
