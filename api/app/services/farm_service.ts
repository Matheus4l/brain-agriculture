// app/Services/FarmService.ts

import Farm from '../models/farm.js'
import db from '@adonisjs/lucid/services/db'

export default class FarmService {
  /**
   * Método para criar uma nova fazenda.
   * @param data - Os dados da nova fazenda a ser criada.
   * @returns A nova fazenda criada.
   */
  async create(data: Partial<Farm>, cultivatedCropsIds: number[]): Promise<Farm | string> {
    if (
      data.agricultural_area_hectares === undefined ||
      data.vegetation_area_hectares === undefined ||
      data.total_area_hectares === undefined ||
      data.agricultural_area_hectares + data.vegetation_area_hectares > data.total_area_hectares
    ) {
      return 'The sum of agricultural area and vegetation area cannot be greater than total area'
    }
    const farm = await Farm.create(data)
    await farm.related('cultivatedCrops').attach(cultivatedCropsIds)
    return farm
  }

  /**
   * Método para atualizar uma fazenda existente.
   * @param id - O ID da fazenda a ser atualizada.
   * @param data - Os dados atualizados da fazenda.
   * @returns A fazenda atualizada.
   */
  async update(id: number, data: Partial<Farm>): Promise<Farm> {
    const farm = await Farm.findOrFail(id)
    farm.merge(data)
    await farm.save()
    return farm
  }

  /**
   * Método para excluir uma fazenda existente.
   * @param id - O ID da fazenda a ser excluída.
   */
  async delete(id: number): Promise<void> {
    const farm = await Farm.findOrFail(id)
    await farm.delete()
  }

  /**
   * Método para buscar todas as fazendas.
   * @returns Uma lista de todas as fazendas.
   */
  async getAll(): Promise<Farm[]> {
    return await Farm.all()
  }

  async getAllPaginated(page: number, perPage: number) {
    // Obtém a lista de produtores rurais paginados do banco de dados
    const farms = await Farm.query().paginate(page, perPage)

    return farms
  }

  /**
   * Método para buscar uma fazenda pelo ID.
   * @param id - O ID da fazenda a ser buscada.
   * @returns A fazenda encontrada.
   */
  async getById(id: number): Promise<Farm> {
    return await Farm.findOrFail(id)
  }

  async getTotalFarms(): Promise<any> {
    const { rows } = await db.rawQuery('SELECT COUNT(*) AS total FROM farms')
    const totalFarms = rows[0]?.total || 0 // para lidar com a possibilidade de rows[0] ser undefined
    return totalFarms
  }

  async getTotalHectares(): Promise<number> {
    const { rows } = await db.rawQuery(
      'SELECT SUM(total_area_hectares) AS total_hectares FROM farms'
    )
    const totalHectares = rows[0]?.total_hectares || 0
    return totalHectares
  }
  async getTotalFarmsByState(): Promise<{ state: string; totalFarms: number }[]> {
    const query = `
      SELECT state_name AS state, COUNT(*)::int AS totalFarms
      FROM farms
      GROUP BY state_name
    `
    const { rows } = await db.rawQuery(query)
    const result = await rows.map((row: any) => ({
      state: row.state,
      totalFarms: row.totalfarms,
    }))

    return result
  }

  async getTotalFarmsByCrop(): Promise<{ crop: string; totalFarms: number }[]> {
    const query = `
      SELECT cc.name AS crop, COUNT(*)::int AS totalFarms
      FROM cultivated_crops cc
      JOIN farm_cultivated_crop_associations fcca ON cc.id = fcca.cultivated_crop_id
      GROUP BY cc.name
    `

    const { rows } = await db.rawQuery(query)
    const result = rows.map((row: any) => ({
      crop: row.crop,
      totalFarms: row.totalfarms,
    }))

    return result
  }

  async getTotalFarmsByLandUse(): Promise<{ landUse: string; totalFarms: number }[]> {
    const query = `
      SELECT
        CASE
          WHEN agricultural_area_hectares > 0 AND vegetation_area_hectares > 0 THEN 'Mixed'
          WHEN agricultural_area_hectares > 0 THEN 'Agricultural'
          WHEN vegetation_area_hectares > 0 THEN 'Vegetation'
          ELSE 'None'
        END AS landUse,
        COUNT(*)::int AS totalFarms
      FROM farms
      GROUP BY landUse
    `

    const { rows } = await db.rawQuery(query)
    const result = rows.map((row: any) => ({
      landUse: row.landuse,
      totalFarms: row.totalfarms,
    }))

    return result
  }
}
