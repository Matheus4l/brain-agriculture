import vine from '@vinejs/vine'

export const createFarmValidator = vine.compile(
  vine.object({
    name: vine.string().trim().minLength(2).maxLength(255),
    total_area_hectares: vine.number().positive(),
    agricultural_area_hectares: vine.number().positive(),
    vegetation_area_hectares: vine.number().positive(),
    rural_producer_id: vine.number().positive(),
  })
)

export const updateFarmValidator = vine.compile(
  vine.object({
    name: vine.string().trim().minLength(2).maxLength(255),
    total_area_hectares: vine.number().positive(),
    agricultural_area_hectares: vine.number().positive(),
    vegetation_area_hectares: vine.number().positive(),
    rural_producer_id: vine.number().positive(),
  })
)
