// app/Validators/RuralProducerValidator.ts

import vine from '@vinejs/vine'

/**
 * Validates the creation of a Rural Producer
 */
export const createRuralProducerValidator = vine.compile(
  vine.object({
    cpf_or_cnpj: vine.string().trim().minLength(11).maxLength(14),
    name: vine.string().trim().minLength(3),
  })
)

/**
 * Validates the update of a Rural Producer
 */
export const updateRuralProducerValidator = vine.compile(
  vine.object({
    name: vine.string().trim().minLength(3),
  })
)
