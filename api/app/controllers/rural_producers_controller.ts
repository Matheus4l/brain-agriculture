// import type { HttpContext } from '@adonisjs/core/http'

// app/Controllers/Http/RuralProducerController.ts

// import RuralProducer from '../models/rural_producer.js'
import { HttpContext } from '@adonisjs/core/http'

import RuralProducerService from '../services/rural_producer_service.js'
import {
  createRuralProducerValidator,
  updateRuralProducerValidator,
} from '#validators/rural_producer'

export default class RuralProducerController {
  private readonly ruralProducerService = new RuralProducerService()

  /**
   * Método para criar um novo produtor rural.
   * @param ctx - O contexto da requisição HTTP.
   * @returns O produtor rural recém-criado.
   */

  async index({ response, request }: HttpContext) {
    try {
      // Define a página padrão e o tamanho da página
      const page = Number.parseInt(request.qs().page) ?? 1
      const perPage = Number.parseInt(request.qs().perPage) ?? 10

      // Obtém a lista de produtores rurais com paginação
      const ruralProducers = await this.ruralProducerService.getAllPaginated(page, perPage)

      // Retorna a lista de produtores rurais paginada
      return response.status(200).json(ruralProducers)
    } catch (error) {
      // Retorna detalhes do erro na resposta
      return response.status(500).json({ error: 'Internal Server Error' })
    }
  }

  async store({ request, response }: HttpContext) {
    const data = request.only(['cpf_or_cnpj', 'name'])

    await request.validateUsing(createRuralProducerValidator)

    const result = await this.ruralProducerService.create(data)

    if (typeof result === 'string') {
      return response.status(400).json({ error: result })
    } else {
      return response.status(201).json(result)
    }
  }

  async update({ request, response, params }: HttpContext) {
    try {
      // Valida os dados de entrada usando o validador de atualização de Rural Producer
      const validatedData = await request.validateUsing(updateRuralProducerValidator)

      // Continua com a lógica de atualização do produtor rural...
      const updatedRuralProducer = await this.ruralProducerService.update(params.id, validatedData)

      // Retorna o produtor rural atualizado
      return response.status(200).json(updatedRuralProducer)
    } catch (error) {
      // Retorna uma resposta de erro com status 400 e mensagens de erro de validação
      return response.status(400).json({ error: error.messages })
    }
  }

  async destroy({ response, params }: HttpContext) {
    try {
      const ruralProducerId = params.id
      const result = await this.ruralProducerService.delete(ruralProducerId)

      if (typeof result === 'string') {
        return response.status(400).json({ error: result })
      } else {
        return response.status(200).json({ message: 'Rural producer deleted successfully' })
      }
    } catch (error) {
      return response.status(500).json({ error: 'Internal Server Error' })
    }
  }
}
