// import type { HttpContext } from '@adonisjs/core/http'

import FarmService from '#services/farm_service'
import { createFarmValidator } from '#validators/farm'
import { HttpContext } from '@adonisjs/core/http'

export default class FarmsController {
  private readonly farmService = new FarmService()

  /**
   * Método para criar uma nova fazenda.
   * @param ctx - O contexto da requisição HTTP.
   * @returns A fazenda recém-criada.
   */
  async store({ request, response }: HttpContext) {
    try {
      // Valida os dados da fazenda
      const data = request.only([
        'name',
        'total_area_hectares',
        'agricultural_area_hectares',
        'vegetation_area_hectares',
        'rural_producer_id',
        'cityName',
        'stateName',
      ])

      createFarmValidator.validate(data)

      const cultivatedCropsIds = request.input('cultivated_crops_ids')

      // Cria a fazenda
      const result = await this.farmService.create(data, cultivatedCropsIds)

      if (typeof result === 'string') {
        return response.status(400).json({ error: result })
      } else {
        return response.status(201).json(result)
      }
    } catch (error) {
      // Retorna uma resposta de erro com status 400 se houver erros de validação
      if (error.isJoi) {
        return response
          .status(400)
          .json({ error: error.details.map((detail: any) => detail.message) })
      }

      // Retorna uma resposta de erro com status 500 se ocorrer um erro inesperado
      console.error('Error occurred:', error)
      return response.status(500).json({ error: 'Internal Server Error' })
    }
  }

  /**
   * Método para atualizar uma fazenda existente.
   * @param ctx - O contexto da requisição HTTP.
   * @returns A fazenda atualizada.
   */
  async update({ params, request, response }: HttpContext) {
    try {
      // Valida os dados da fazenda
      //   await request.validateUsing(updateFarmValidator)

      const data = request.only([
        'name',
        'total_area_hectares',
        'agricultural_area_hectares',
        'vegetation_area_hectares',
        'rural_producer_id',
      ])

      // Valida os dados da fazenda
      //   await updateFarmValidator.validate(data)
      // Atualiza a fazenda
      const farm = await this.farmService.update(params.id, data)

      // Retorna a fazenda atualizada
      return response.status(200).json(farm)
    } catch (error) {
      // Retorna uma resposta de erro com status 400 se houver erros de validação
      if (error.isJoi) {
        return response
          .status(400)
          .json({ error: error.details.map((detail: any) => detail.message) })
      }

      // Retorna uma resposta de erro com status 500 se ocorrer um erro inesperado
      console.error('Error occurred:', error)
      return response.status(500).json({ error: 'Internal Server Error' })
    }
  }

  /**
   * Método para excluir uma fazenda existente.
   * @param ctx - O contexto da requisição HTTP.
   * @returns Uma mensagem de sucesso.
   */
  async destroy({ params, response }: HttpContext) {
    try {
      // Exclui a fazenda
      await this.farmService.delete(params.id)

      // Retorna uma mensagem de sucesso
      return response.status(200).json({ message: 'Farm deleted successfully' })
    } catch (error) {
      // Retorna uma resposta de erro com status 500 se ocorrer um erro inesperado
      console.error('Error occurred:', error)
      return response.status(500).json({ error: 'Internal Server Error' })
    }
  }

  /**
   * Método para listar todas as fazendas.
   * @param ctx - O contexto da requisição HTTP.
   * @returns Uma lista de fazendas.
   */
  async index({ response, request }: HttpContext) {
    try {
      // Obtém todas as fazendas
      const page = Number.parseInt(request.qs().page) ?? 1
      const perPage = Number.parseInt(request.qs().perPage) ?? 10

      const farms = await this.farmService.getAllPaginated(page, perPage)

      // Retorna a lista de fazendas
      return response.status(200).json(farms)
    } catch (error) {
      // Retorna uma resposta de erro com status 500 se ocorrer um erro inesperado
      console.error('Error occurred:', error)
      return response.status(500).json({ error: 'Internal Server Error' })
    }
  }
}
