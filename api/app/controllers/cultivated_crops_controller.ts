// import type { HttpContext } from '@adonisjs/core/http'

import { HttpContext } from '@adonisjs/core/http'
import CultivatedCropService from '#services/cultivated_crop_service'

export default class CultivatedCropsController {
  private readonly cultivatedCropService = new CultivatedCropService()
  /**
   * Método para criar um novo produtor rural.
   * @param ctx - O contexto da requisição HTTP.
   * @returns O Culturas plantadas recém-criado.
   */

  async index({ response }: HttpContext) {
    try {
      const ruralProducers = await this.cultivatedCropService.getAll()

      return response.status(200).json(ruralProducers)
    } catch (error) {
      return response.status(500).json({ error: 'Internal Server Error' })
    }
  }
}
