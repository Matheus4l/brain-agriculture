// import type { HttpContext } from '@adonisjs/core/http'

import FarmService from '#services/farm_service'
import { HttpContext } from '@adonisjs/core/http'

export default class DashboardController {
  private farmService = new FarmService()

  async dashboard({ response }: HttpContext) {
    const totalFarms = await this.farmService.getTotalFarms()
    const totalFarmArea = await this.farmService.getTotalHectares()
    const totalFarmsByState = await this.farmService.getTotalFarmsByState()
    const totalFarmsByCrop = await this.farmService.getTotalFarmsByCrop()
    const totalFarmsByandUse = await this.farmService.getTotalFarmsByLandUse()

    return response
      .status(200)
      .json({ totalFarms, totalFarmArea, totalFarmsByState, totalFarmsByCrop, totalFarmsByandUse })
  }
}
