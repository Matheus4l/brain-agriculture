import { BaseSchema } from '@adonisjs/lucid/schema'

export default class extends BaseSchema {
  protected tableName = 'farm_cultivated_crop_associations'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.integer('farm_id').unsigned().references('id').inTable('farms').onDelete('CASCADE')
      table
        .integer('cultivated_crop_id')
        .unsigned()
        .references('id')
        .inTable('cultivated_crops')
        .onDelete('CASCADE')
      // Outras colunas necessárias
      table.timestamps(true, true)
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
