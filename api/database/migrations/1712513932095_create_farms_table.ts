import { BaseSchema } from '@adonisjs/lucid/schema'

export default class extends BaseSchema {
  protected tableName = 'farms'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.string('name').notNullable()
      table
        .integer('rural_producer_id')
        .unsigned()
        .references('id')
        .inTable('rural_producers')
        .onDelete('CASCADE')
      table.decimal('total_area_hectares').notNullable()
      table.decimal('agricultural_area_hectares').notNullable()
      table.decimal('vegetation_area_hectares').notNullable()
      table.string('city_name').notNullable() // Adiciona o campo para o nome da cidade
      table.string('state_name').notNullable()
      table.timestamps(true, true)
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
