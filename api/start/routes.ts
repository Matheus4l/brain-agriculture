/*
|--------------------------------------------------------------------------
| Routes file
|--------------------------------------------------------------------------
|
| The routes file is used for defining the HTTP routes.
|
*/

const RuralProducerController = () => import('#controllers/rural_producers_controller')
const FarmsController = () => import('#controllers/farms_controller')

const CultivatedCropsController = () => import('#controllers/cultivated_crops_controller')
const DashboardController = () => import('#controllers/dashboard_controller')
import router from '@adonisjs/core/services/router'

router
  .group(() => {
    router.get('/', async () => {
      return {
        hello: 'world',
      }
    })
    //Rural Producer
    router.get('/rural-producers', [RuralProducerController, 'index'])
    router.post('/rural-producer', [RuralProducerController, 'store'])
    router.put('/rural-producer/:id', [RuralProducerController, 'update'])
    router.delete('/rural-producer/:id', [RuralProducerController, 'destroy'])

    //Farm
    router.get('/farms', [FarmsController, 'index'])
    router.post('/farm', [FarmsController, 'store'])
    router.put('/farm/:id', [FarmsController, 'update'])
    router.delete('/farm/:id', [FarmsController, 'destroy'])

    router.get('/cultivated-crops', [CultivatedCropsController, 'index'])

    router.get('/dashboard', [DashboardController, 'dashboard'])
  })
  .prefix('/api')
