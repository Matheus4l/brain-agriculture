# Use uma imagem de node como base
FROM node:20

# Defina o diretório de trabalho dentro do contêiner
WORKDIR /usr/src/app

# Copie o package.json e o package-lock.json para o diretório de trabalho
COPY api/package*.json ./

# Instale as dependências do projeto
RUN npm install

# Copie o restante dos arquivos do projeto para o diretório de trabalho
COPY api .

# Exponha a porta 3333 para que o aplicativo AdonisJS possa ser acessado externamente
EXPOSE 1379

# Comando para iniciar o servidor AdonisJS
CMD ["npm", "run", "dev"]
