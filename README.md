## Sobre


Projeto Exemplo em AdonisJS
Este é um projeto de exemplo utilizando o framework AdonisJS. Ele serve como um ponto de partida para o desenvolvimento de aplicações web utilizando AdonisJS.


## Requisitos

Requisitos
Node.js >= 14.x
npm >= 6.x ou yarn >= 1.x
AdonisJS CLI

### Instalação

Clone o repositório:
```
git clone https://github.com/Matheus4l/brain-agriculture.git
```
Navegue até o diretório do projeto:
```
cd brain-agriculture/api
```
Instale as dependências:

```
npm install
```
